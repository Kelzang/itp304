def set_i(n, k):
    mask = 1 << k
    n = n | mask
    return n

print(set_i(13,1))
print(set_i(13,2))