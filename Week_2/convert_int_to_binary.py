def int_to_binary(num):
    binary = list()
    while(num > 0):
        lastbit = num & 1
        num = num >> 1
        binary.append(str(lastbit))
    return binary[::-1]
    # return "".join(binary[::-1])

print(int_to_binary(13))    